String.toLocaleString({
  'en': {
    'ShowMore' : 'More',
    'ShowLess': 'Less',
    'ClearAllFilters' : 'Clear All',
    'Product Help & Support' : 'Products',
    'Others' : 'Others',
    'NoResult' : 'Sorry. We weren\'t able to find any results for your search.',
    'NoResultTips1' : 'Please make sure the words are correct.',
    'NoResultTips2' : 'Try entering a similar word.',
    'NoResultTips3' : 'Please use \'Did you mean or \'RELATED SEARCHES\' to find the result you want.'
  }
});

