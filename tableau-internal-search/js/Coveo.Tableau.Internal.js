(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["CoveoExtension"] = factory();
	else
		root["CoveoExtension"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/js/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	var _this = this;
	// export let Coveo = require('coveo-search-ui');
	var CustomEvents_1 = __webpack_require__(2);
	exports.CustomEvents = CustomEvents_1.CustomEvents;
	__export(__webpack_require__(3));
	var TableauHelper_1 = __webpack_require__(5);
	exports.TableauHelper = TableauHelper_1.TableauHelper;
	var TableauTrustStatus_1 = __webpack_require__(9);
	exports.TableauTrustStatus = TableauTrustStatus_1.TableauTrustStatus;
	// export {TableauDependentFacetManager} from './custo/TableauDependentFacetManager';
	var TableauCustoInternal_1 = __webpack_require__(8);
	exports.TableauCustoInternal = TableauCustoInternal_1.TableauCustoInternal;
	var FeedbackLink_1 = __webpack_require__(10);
	exports.FeedbackLink = FeedbackLink_1.FeedbackLink;
	var HomeTab_1 = __webpack_require__(11);
	exports.HomeTab = HomeTab_1.HomeTab;
	var CustomPrintableUri_1 = __webpack_require__(12);
	exports.CustomPrintableUri = CustomPrintableUri_1.CustomPrintableUri;
	// export {Feedback} from './ui/Feedback/Feedback';
	// export {RelatedLinks} from './ui/RelatedLinks/RelatedLinks'
	// export {AbstractRelatedItems} from './ui/RelatedLinks/AbstractRelatedItems'
	// export {RelatedItems} from './ui/RelatedLinks/RelatedItems'
	// export {RelatedLanguages} from './ui/RelatedLinks/RelatedLanguages'
	// export {CaseContext} from './ui/CaseContext/CaseContext'
	// export {RelatedLinks2} from './ui/RelatedLinks/RelatedLinks2'
	// Webpack output a library target with a temporary name.
	// This is to allow end user to put CoveoPSComponents.js before or after the main CoveoJsSearch.js, without breaking
	// This code swap the current module to the "real" Coveo variable.
	var swapVar = function () {
	    if (window['Coveo'] == undefined) {
	        window['Coveo'] = _this;
	    }
	    else {
	        _.each(_.keys(_this), function (k) {
	            window['Coveo'][k] = _this[k];
	        });
	    }
	};
	swapVar();


/***/ }),
/* 2 */
/***/ (function(module, exports) {

	"use strict";
	var CustomEvents = (function () {
	    function CustomEvents() {
	    }
	    CustomEvents.sendEmail = 'tableauSendEmail';
	    CustomEvents.populateContextBoxSuggestions = 'populateContextBoxSuggestions';
	    CustomEvents.gatheringContext = 'gatheringContext';
	    return CustomEvents;
	}());
	exports.CustomEvents = CustomEvents;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var TableauCusto_1 = __webpack_require__(4);
	var TableauCustoInternal_1 = __webpack_require__(8);
	// export function configureTableauEndpoint(token?: string, options: any = {}) {
	//   console.log(process.env.NODE_ENV);
	//   Coveo.SearchEndpoint.configureCloudEndpoint(config.coveo.org_id, token || config.coveo.api_key, undefined, options);
	// }
	function initTableauCustoInternal(element, options) {
	    if (options === void 0) { options = {}; }
	    //let preInitcusto = new TableauPreInitialization();
	    //let promiseOptions = preInitcusto.getAsyncOptions();
	    //promiseOptions.then((asyncOpts) => {
	    //let preInitoptions = _.extend(options, asyncOpts);
	    Coveo.Initialization.initializeFramework(element, options, function () {
	        var custo = new TableauCustoInternal_1.TableauCustoInternal(element);
	        var tableauOpt = _.extend(options, custo.getDefaultOptions());
	        Coveo.Initialization.initSearchInterface(element, tableauOpt);
	    });
	    //});  
	}
	exports.initTableauCustoInternal = initTableauCustoInternal;
	function initTableauCusto(element, options) {
	    if (options === void 0) { options = {}; }
	    Coveo.Initialization.initializeFramework(element, options, function () {
	        // Coveo.SearchEndpoint.configureSampleEndpoint();
	        // Coveo.SearchEndpoint.configureCloudEndpoint(config.coveo.org_id, config.coveo.api_key);
	        var custo = new TableauCusto_1.TableauCusto(element);
	        // custo.initStrings();
	        var tableauOpt = _.extend(options, custo.getDefaultOptions());
	        Coveo.Initialization.initSearchInterface(element, tableauOpt);
	    });
	}
	exports.initTableauCusto = initTableauCusto;
	function initTableauBoxCusto(element, options) {
	    if (options === void 0) { options = {}; }
	    Coveo.Initialization.initializeFramework(element, options, function () {
	        // Coveo.SearchEndpoint.configureSampleEndpoint();
	        // Coveo.SearchEndpoint.configureCloudEndpoint(config.coveo.org_id, config.coveo.api_key);
	        // let custo = new TableauCusto(element);
	        var custo = new TableauCusto_1.TableauCusto(element);
	        // custo.initStrings();
	        var tableauOpt = _.extend(options, custo.getDefaultOptions());
	        Coveo.Initialization.initBoxInterface(element, tableauOpt);
	    });
	}
	exports.initTableauBoxCusto = initTableauBoxCusto;
	Coveo.Initialization.registerNamedMethod('initTableauCusto', function (element, options) {
	    if (options === void 0) { options = {}; }
	    initTableauCusto(element, options);
	});
	Coveo.Initialization.registerNamedMethod('initTableauBoxCusto', function (element, options) {
	    if (options === void 0) { options = {}; }
	    initTableauBoxCusto(element, options);
	});


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var $$ = Coveo.$$;
	var CustomEvents_1 = __webpack_require__(2);
	var TableauHelper_1 = __webpack_require__(5);
	var TableauDependentFacetManager_1 = __webpack_require__(7);
	/**
	 * Required customization specifically applied for Tableau's implementation
	 */
	var TableauCusto = (function () {
	    function TableauCusto(searchInterfaceElement) {
	        var _this = this;
	        this.searchInterfaceElement = searchInterfaceElement;
	        this.rootElement = $$(searchInterfaceElement);
	        // let changeTabEvtName = Coveo.QueryStateModel.eventTypes.changeOne + Coveo.QueryStateModel.attributesEnum.t;
	        var changeFacetSourceEvtNames = [
	            this.getStateEventName(Coveo.QueryStateModel.eventTypes.changeOne + 'f:allSource'),
	            this.getStateEventName(Coveo.QueryStateModel.eventTypes.changeOne + 'f:coreSource')
	        ];
	        var changeAllFacetsEvtNames = _.map(this.rootElement.findAll('.CoveoFacet'), function (el) {
	            return _this.getStateEventName(Coveo.QueryStateModel.eventTypes.changeOne + 'f:' + el.dataset['id']);
	        });
	        // Initialization Events
	        this.rootElement.on(Coveo.InitializationEvents.beforeInitialization, function () { return _this.handleBeforeInit(); });
	        this.rootElement.on(Coveo.InitializationEvents.afterInitialization, function () { return _this.handleAfterInit(); });
	        this.rootElement.on(Coveo.InitializationEvents.afterComponentsInitialization, function () { return _this.handleAfterComponentsInit(); });
	        // Query Events
	        this.rootElement.on(Coveo.QueryEvents.newQuery, function (e, data) { return _this.handleNewQuery(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.buildingQuery, function (e, data) { return _this.handleBuildingQuery(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.doneBuildingQuery, function (e, data) { return _this.handleDoneBuildingQuery(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.preprocessResults, function (e, data) { return _this.handlePreprocessResults(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.querySuccess, function (e, data) { return _this.handleQuerySuccess(e, data); });
	        // this.rootElement.on('attachToCase', (e: Event, data: any) => this.handleAttachToCase(e, data));
	        // State Events
	        // this.rootElement.on(this.getStateEventName(changeTabEvtName), (e: Event, data: IAttributeChangedEventArg) => this.handleTabStateChanged(e, data));
	        // this.rootElement.on(changeFacetSourceEvtNames, (e: Event, data: IAttributeChangedEventArg) => this.handleFacetSourceStateChanged(e, data));
	        this.rootElement.on(changeAllFacetsEvtNames, function (e, data) { return _this.handleAllFacetsStateChanged(e, data); });
	        // Custom Events
	        this.rootElement.on(CustomEvents_1.CustomEvents.sendEmail, function (e, data) { return _this.handleSendEmail(e, data); });
	        this.rootElement.on(CustomEvents_1.CustomEvents.populateContextBoxSuggestions, function (e, data) { return _this.handleContextBoxSuggestion(e, data); });
	        this.rootElement.on(CustomEvents_1.CustomEvents.gatheringContext, function (e, data) { return _this.handleGatheringContext(e, data); });
	    }
	    TableauCusto.prototype.getStateEventName = function (event) {
	        return Coveo.QueryStateModel.ID + ':' + event;
	    };
	    /**
	     * Before Initialization
	     */
	    TableauCusto.prototype.handleBeforeInit = function () {
	    };
	    /**
	     * After Initialization
	     * initializing custom strings during before init event to avoid any issue with SFDC init strings.
	     */
	    TableauCusto.prototype.handleAfterInit = function () {
	        this.initStrings();
	    };
	    /**
	     * After Component Initialization
	     * Adding Setting Menu item for Tableau training link
	     * Registering custom template helper to manage tableau custom icons. see result templates
	     */
	    TableauCusto.prototype.handleAfterComponentsInit = function () {
	        this.searchInterface = Coveo.Component.get(this.rootElement.el, Coveo.SearchInterface);
	        this.rootElement.on(Coveo.SettingsEvents.settingsPopulateMenu, function (e, args) {
	            args.menuData.push({
	                className: 'tableau-training-link',
	                text: Coveo.l('TableauTrainingDoc'),
	                onOpen: function () { window.open('https://mytableau.tableaucorp.com/x/SeMoBg', '_blank'); },
	                onClose: function () { }
	            });
	        });
	        Coveo.TemplateHelpers.registerTemplateHelper('fromTableauTypeToIcon', function (result, options) {
	            return TableauHelper_1.TableauHelper.fromTableauTypeToIcon(result, options);
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('customDate', function (value, options) {
	            return TableauHelper_1.TableauHelper.customDate(Coveo.DateUtils.convertFromJsonDateIfNeeded(value));
	        });
	        this.updateNbAttachedResults();
	    };
	    /**
	     * New Query
	     */
	    TableauCusto.prototype.handleNewQuery = function (evt, args) {
	        var q = this.searchInterface.queryStateModel.get('q');
	        var txt = q ? ' for "' + q + '"' : '';
	        var qryEntryEl = $$(this.searchInterfaceElement).find('.query-entry-placeholder');
	        if (qryEntryEl) {
	            qryEntryEl.innerText = txt;
	        }
	        TableauDependentFacetManager_1.TableauDependentFacetManager.handleDependentFacets();
	    };
	    /**
	     * Building Query
	     */
	    TableauCusto.prototype.handleBuildingQuery = function (evt, args) { };
	    /**
	     * Done Building Query
	     */
	    TableauCusto.prototype.handleDoneBuildingQuery = function (evt, args) { };
	    /**
	     * Preprocess Results
	     */
	    TableauCusto.prototype.handlePreprocessResults = function (evt, args) {
	        _.each(args.results.results, function (result) {
	            _.extend(result, {
	                isCaseContextActivated: TableauHelper_1.TableauHelper.isCaseContextActivated()
	            });
	            // workaround to fix attach to case issue ...
	            if (result.raw.sfknowledgebaseversionnumber) {
	                result.raw.sfkbversionnumber = result.raw.sfknowledgebaseversionnumber;
	            }
	        });
	    };
	    TableauCusto.prototype.handleQuerySuccess = function (evt, args) {
	        // if not a Coveo.Box...
	        if (!this.searchInterface['boxStateModel']) {
	            var attachedResultsTabEl = this.rootElement.find('.CoveoAttachedResultsTab');
	            if (attachedResultsTabEl && TableauHelper_1.TableauHelper.isCaseContextActivated()) {
	                attachedResultsTabEl.style.display = 'inline-block';
	            }
	            else if (attachedResultsTabEl) {
	                attachedResultsTabEl.style.display = 'none';
	            }
	            this.updateNbAttachedResults();
	        }
	    };
	    /**
	     * Send Email sample - Not used anymore.
	     */
	    TableauCusto.prototype.handleSendEmail = function (evt, args) {
	        var obj = args;
	        console.log(['send email >>> '].concat(obj));
	    };
	    TableauCusto.prototype.handleContextBoxSuggestion = function (evt, args) {
	        var _self = this;
	        var promise = new Promise(function (resolve, reject) {
	            // setTimeout(() => {
	            var currentText = args.caseContext.magicBox.getText();
	            // Coveo.SearchEndpoint.endpoints['testjfa'].search({
	            Coveo.SearchEndpoint.endpoints['default'].search({
	                q: currentText,
	                cq: args.caseContext.options.suggestionConstantExpression,
	                wildcards: true,
	                partialMatch: true
	            }).done(function (data) {
	                var results = _.map(data.results, function (r) {
	                    var suggestionHtml = args.caseContext.options.suggestionTemplate.instantiateToString(_.extend(r, { currentText: currentText }));
	                    return {
	                        html: Coveo.StreamHighlightUtils.highlightStreamHTML(suggestionHtml, r.termsToHighlight, r.phrasesToHighlight),
	                        onSelect: function () {
	                            window.location.href = (window.location.pathname + '?caseId=' + r.raw.sfcaseid + Coveo.HashUtils.getHash() + '&hd=Context');
	                            // Coveo.state(_self.rootElement.el, 'caseId', r.raw.sfcaseid);
	                            // Coveo.executeQuery(_self.rootElement.el);
	                        }
	                    };
	                });
	                console.log(results);
	                resolve(results);
	            });
	            // }, 20)
	        });
	        args.suggestions = [promise];
	    };
	    TableauCusto.prototype.handleGatheringContext = function (evt, args) {
	        if (args.caseResult) {
	            args.context = {
	                'Subject': args.caseResult.raw.sfcasesubject,
	                'Created By': args.caseResult.raw.sfcreatedby + ', ' + Coveo.DateUtils.dateToString(new Date(args.caseResult.raw.sfcreateddate), { predefinedFormat: 'MMM dd yyyy' }),
	                'Last Modified By': args.caseResult.raw.sflastmodifiedbyname + ', ' + Coveo.DateUtils.dateToString(new Date(args.caseResult.raw.sflastmodifieddate), { predefinedFormat: 'MMM dd yyyy' }),
	                'Bug Number Link': args.caseResult.raw.sfcasebugnumberlinkc,
	                'Web Name': args.caseResult.raw.sfcasesuppliedname
	            };
	        }
	    };
	    TableauCusto.prototype.handleAttachToCase = function (evt, args) {
	        args.dataToAttach.articleVersionNumber = args.dataToAttach.articleVersionNumber || args.result.raw.sfknowledgebasearticlenumber;
	    };
	    TableauCusto.prototype.updateNbAttachedResults = function () {
	        if (window['attachToCaseEndpoint']) {
	            var nbAttachedResults = window['attachToCaseEndpoint'].data.attachedResults.length;
	            var nbAttachedResultsEl = document.querySelector('.CoveoNbAttachedResults');
	            var attachedResultsTab = Coveo.get(this.rootElement.find('.CoveoAttachedResultsTab'));
	            if (attachedResultsTab) {
	                if (nbAttachedResultsEl) {
	                    Coveo.$$(nbAttachedResultsEl).remove();
	                }
	                Coveo.$$(attachedResultsTab.element).append(Coveo.$$('p', { className: 'CoveoNbAttachedResults' }, '(' + nbAttachedResults + ')').el);
	            }
	        }
	    };
	    /**
	     * Tab state changed - Not used for phase 1
	     * - `Event`: event
	     * - `args`: AttributeChangedEventArg
	     */
	    // private handleTabStateChanged(evt: Event, args: IAttributeChangedEventArg) {
	    //   var currentTab = this.rootElement.find('.CoveoTab[data-id="' + args.value + '"]');
	    //   var mainTabTitle: HTMLElement = <HTMLElement>document.querySelector('.coveo-current-tab');
	    //   if (mainTabTitle) {
	    //     $$(mainTabTitle).text($$(currentTab).getAttribute('data-caption'));
	    //   }
	    // }
	    /**
	     * Facet commonSource state changed - NOT NEEDED anymore [01/04/2017]
	     * This is to force an exclude value Archived on publishStatus facet when Knowledge Base value is
	     * selected in CommonSource facet.
	     * - `Event`: event
	     * - `args`: AttributeChangedEventArg
	     */
	    // private handleFacetSourceStateChanged(evt: Event, args: IAttributeChangedEventArg) {
	    //   var selectedValues: Array<string> = args.value;
	    //   var publishStatusFacets = document.querySelectorAll('.CoveoFacet[data-field="@sfkbpublishstatus"]');
	    //   _.each(publishStatusFacets, (publishFacet) => {
	    //     var el: HTMLElement = <HTMLElement>publishFacet;
	    //     var facet = <Coveo.Facet>Coveo.Component.get(el, Coveo.Facet);
	    //     if (selectedValues.indexOf('Knowledge Base') >= 0) {
	    //       facet.excludeValue('Archived');
	    //     } else {
	    //       facet.reset();
	    //     }
	    //   });
	    // }
	    TableauCusto.prototype.handleAllFacetsStateChanged = function (evt, args) {
	        var selectedValues = args.value;
	        var id = args.attribute.replace('f:', '');
	        if (selectedValues.length) {
	            var facetEl = document.querySelector('.CoveoFacet[data-id="' + id + '"]');
	            var facetCmp = Coveo.Component.get(facetEl, Coveo.Facet);
	            var previousValues = Coveo.HashUtils.getValue(args.attribute, Coveo.HashUtils.getHash()) || [];
	            if (facetCmp.getDisplayedValues().indexOf('Not Specified') >= 0 && previousValues.indexOf('Not Specified') < 0) {
	                facetCmp.selectValue('Not Specified');
	            }
	        }
	    };
	    /**
	     * Initialized Custom Strings for Tableau
	     */
	    TableauCusto.prototype.initStrings = function () {
	        // Custom variable for current application
	        String.toLocaleString({
	            'en': {
	                'ShowingResultsOf': '{2} result<pl>s</pl>',
	                'TableauFeedback': 'Give Feedback',
	                'TableauTrainingDoc': 'Training',
	                'SelectYourCase': 'Search Cases...',
	                'SmartCaseFinder': 'Attach to case',
	                'NoRelatedItemsFound': 'No Related {0} found',
	                'NoRelatedCasesFound': 'No Support cases found',
	                'RelatedSupportCases': 'Support Cases',
	                'RelatedKBs': 'External Links',
	                'TranslatedLinks': 'Translated Links',
	                'OopsError': 'Sorry, something went wrong',
	                'ProblemPersists': ''
	            }
	        });
	    };
	    /**
	     * Set default options of different UI Components for Tableau.
	     */
	    TableauCusto.prototype.getDefaultOptions = function () {
	        var tableauOptions = {
	            SalesforceResultLink: {
	                alwaysOpenInNewWindow: true
	            },
	            ResultLink: {
	                alwaysOpenInNewWindow: true
	            },
	            Facet: {
	                availableSorts: ['occurrences', 'score', 'alphaAscending', 'alphaDescending'],
	                valueCaption: {
	                    'Not Specified': 'Unspecified',
	                    'Web Misc.': 'Web (other)'
	                }
	            }
	        };
	        return tableauOptions;
	    };
	    return TableauCusto;
	}());
	exports.TableauCusto = TableauCusto;
	;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var CaseContext_1 = __webpack_require__(6);
	var SOURCE_ICONS_MAPPING = {
	    'help': 'icon-help',
	    'knowledge base': 'icon-knowledge-base',
	    'whitepaper': 'icon-whitepaper',
	    'video': 'icon-video-play-icon',
	    'community': 'icon-community',
	    'devhelp': 'icon-devhelp',
	    'known issue': 'icon-known-issue',
	    'idea': 'icon-community-ideas',
	    'release notes': 'icon-release-note',
	    'release note': 'icon-release-note',
	    'support case': 'icon-support-case',
	    'wiki': 'icon-wiki',
	    'tfs defect': 'icon-defect',
	    'web misc.': 'icon-web-other',
	    'unknown': 'CoveoIcon'
	};
	var FILE_TYPE_ICONS_MAPPING = {
	    'pdf': 'enterprise-icon-file_pdf',
	    'xls': 'enterprise-icon-file_excel',
	    'ppt': 'enterprise-icon-file_powerpoint',
	    'doc': 'enterprise-icon-file_word',
	    'html': 'enterprise-icon-file_webpage',
	    'one': 'enterprise-icon-file_onenote',
	    'image': 'enterprise-icon-image',
	    'twbx': 'enterprise-icon-file_twbx',
	    'unknown': 'CoveoIcon'
	};
	var FILE_TYPE_ICONS_MAPPING_INTERNAL = {
	    'pdf': 'icon-file_pdf',
	    'xls': 'icon-file_excel',
	    'ppt': 'icon-file_powerpoint',
	    'doc': 'icon-file_word',
	    'html': 'icon-file_webpage',
	    'one': 'icon-file_onenote',
	    'onetoc2': 'icon-file_onenote',
	    'twbx': 'icon-file_twbx',
	    'xml': 'icon-whitepaper',
	    'zip': 'icon-whitepaper',
	    'jpg': 'icon-whitepaper',
	    'image': 'icon-whitepaper',
	    'png': 'icon-whitepaper',
	    'svg': 'icon-whitepaper',
	    'msg': 'icon-whitepaper',
	    'csdocument': 'icon-whitepaper',
	    'csdiscussion': 'icon-community',
	    'csmessage': 'icon-community',
	    'cscomment': 'icon-community',
	    'unknown': 'icon-file_webpage'
	};
	var TOOLTIP_MAPPING = {
	    'xls': 'Excel document',
	    'one': 'OneNote',
	    'ppt': 'PowerPoint document',
	    'pdf': 'PDF document',
	    'twb': 'Tableau workbook',
	    'twbx': 'Tableau workbook',
	    'doc': 'Word document',
	    'mp4': 'Video',
	    'avi': 'Video',
	    'flv': 'Video',
	    'wmv': 'Video',
	    'mov': 'Video',
	    'zip': 'File or document',
	    'svg': 'File or document',
	    'xml': 'File or document',
	    'image': 'File or document',
	    'msg': 'File or document',
	    'csdocument': 'File or document',
	    'csdiscussion': 'Community discussion',
	    'cscomment': 'Community discussion',
	    'csmessage': 'Community discussion',
	};
	var FACET_VALUES_CAPTION_INTERNAL = {
	    'Not Specified': 'Unspecified',
	    'Web Misc.': 'Web (other)',
	    'csmessage': 'Forum post reply',
	    'csdiscussion': 'Forum post',
	    'cscomment': 'Comment',
	    'csblogpost': 'Blog Post',
	    'csdocument': 'Community Document',
	    'csannouncement': 'Announcement',
	    'cspoll': 'Poll',
	    'cscommunity': 'Commumity',
	    'cssocialgroup': 'Social Group',
	    'html': 'Webpage',
	    'pdf': 'Pdf',
	    'xml': 'Xml',
	    'zip': 'Zip',
	    'xls': 'Spreadsheet',
	    'ppt': 'Presentation',
	    'one': 'OneNote',
	    'onetoc2': 'Onetoc2',
	    'vsd': 'Visio',
	    'twbx': 'Tableau Workbook',
	    'Community': 'Community (Internal)',
	    'speventlist': 'Calendar',
	    '.xml': 'Xml',
	    '1': '1-Critical',
	    '2': '2-High',
	    '3': '3-Medium',
	    '4': '4-Low',
	    '5': '5-Nice to have'
	};
	var FACET_TYPE_ALLOWED_VALUE_INTERNAL = [
	    'Image',
	    'doc',
	    'twb',
	    'twbx',
	    'xls',
	    'Zip',
	    'pdf',
	    'ppt',
	    'txt',
	    'html',
	    'Room',
	    'Person',
	    'View',
	    'Workbook',
	    'Datasource',
	    'one',
	    'vsd',
	    'spsite',
	    'spdocumentlibrarylist',
	    'speventlist',
	    'xml',
	    'csdiscussion',
	    'csmessage',
	    'cscomment',
	    'csblogpost',
	    'csdocument',
	    'csannouncement',
	    'cspoll',
	    'cscommunity',
	    'cssocialgroup',
	    'onetoc2',
	    'Video'
	];
	/* const SOURCE_ICONS_MAPPING_INTERNAL: any = {
	  'help': 'icon-help',
	  'knowledge base': 'icon-knowledge-base',
	  'whitepaper': 'icon-whitepaper',
	  'video': 'icon-video-play-icon',
	  'community': 'icon-community',
	  'devhelp': 'icon-devhelp',
	  'known issue': 'icon-known-issue',
	  'idea': 'icon-community-ideas',
	  'release notes': 'icon-release-note',
	  'release note': 'icon-release-note',
	  'support case': 'icon-support-case',
	  'wiki': 'icon-wiki',
	  'tfs defect': 'icon-defect',
	  'web misc.': 'icon-web-other',
	  'unknown': 'CoveoIcon'
	};
	*/
	var ITableauIconOptions = (function () {
	    function ITableauIconOptions() {
	        this.value = null;
	        this.additionalClass = null;
	    }
	    return ITableauIconOptions;
	}());
	exports.ITableauIconOptions = ITableauIconOptions;
	var TableauHelper = (function () {
	    function TableauHelper() {
	    }
	    TableauHelper.fromTableauFileTypeToIcon = function (result, options) {
	        var iconCss = options.value;
	        if (Coveo.Utils.isNullOrEmptyString(iconCss)) {
	            var type = (result.raw.filetype || '').toLowerCase();
	            iconCss = FILE_TYPE_ICONS_MAPPING[type || 'unknown'];
	            iconCss = !Coveo.Utils.isNullOrEmptyString(options.additionalClass) ? (iconCss + ' ' + options.additionalClass) : iconCss;
	        }
	        return Coveo.$$('i', {
	            className: iconCss,
	            'aria-hidden': true
	        }).el.outerHTML;
	    };
	    TableauHelper.getTypeAllowedValues = function () {
	        return FACET_TYPE_ALLOWED_VALUE_INTERNAL;
	    };
	    TableauHelper.getFacetValueCaptionInternal = function () {
	        return FACET_VALUES_CAPTION_INTERNAL;
	    };
	    //static filterFacetValues(results: Array<Coveo.IIndexFieldValue>): Array<String> {
	    //  var resultsFiltered = [];
	    //  _.each(results, function(res) {
	    //    if (!_.contains(FACET_VALUE_TO_EXCLUDE, res.value)) {
	    //      resultsFiltered.push(res.value)
	    //    }
	    //  });
	    //  return resultsFiltered;
	    // }
	    TableauHelper.fromTableauTypeToIcon = function (result, options) {
	        var iconCss = options.value;
	        var hoverLabel = result.raw.commonsource || '';
	        if (Coveo.Utils.isNullOrEmptyString(iconCss)) {
	            var commonSrc = (result.raw.commonsource || '').toLowerCase();
	            iconCss = SOURCE_ICONS_MAPPING[commonSrc.toLowerCase()] || SOURCE_ICONS_MAPPING['unknown'];
	            if (commonSrc === 'knowledge base') {
	                iconCss = TableauHelper.isAPublicKB(result) ? 'icon-external-knowledge-base' : 'icon-internal-knowledge-base';
	                hoverLabel = TableauHelper.isAPublicKB(result) ? 'External Knowledge Base' : 'Internal Knowledge Base';
	            }
	            else if (commonSrc === 'community') {
	                var question = result.raw.jivequestion ? result.raw.jivequestion.toLowerCase() : '';
	                iconCss = (question === 'true' && result.raw.jiveanswer != null) ? 'icon-community-discussion-answered' : iconCss;
	                iconCss = (question === 'true' && (!result.raw.jiveanswer || result.raw.jiveanswer === 'False')) ? 'icon-community-discussion-unanswered' : iconCss;
	            }
	            iconCss = !Coveo.Utils.isNullOrEmptyString(options.additionalClass) ? (iconCss + ' ' + options.additionalClass) : iconCss;
	        }
	        return Coveo.$$('i', {
	            className: iconCss,
	            title: hoverLabel,
	            'aria-hidden': true
	        }).el.outerHTML;
	    };
	    TableauHelper.fromTableauFileTypeToIconInternal = function (result, options) {
	        var iconCss = options.value;
	        if (Coveo.Utils.isNullOrEmptyString(iconCss)) {
	            var type = (result.raw.filetype || '').toLowerCase();
	            iconCss = !Coveo.Utils.isNullOrEmptyString(FILE_TYPE_ICONS_MAPPING_INTERNAL[type]) ? FILE_TYPE_ICONS_MAPPING_INTERNAL[type] : 'icon-file_webpage';
	            iconCss = TableauHelper.isATwbx(result) ? 'icon-file_twbx' : iconCss;
	            iconCss = !Coveo.Utils.isNullOrEmptyString(options.additionalClass) ? (iconCss + ' ' + options.additionalClass) : iconCss;
	        }
	        var hoverLabel = !Coveo.Utils.isNullOrEmptyString(TOOLTIP_MAPPING[type]) ? TOOLTIP_MAPPING[type] : 'Webpage';
	        hoverLabel = TableauHelper.isATwbx(result) ? 'Tableau workbook' : hoverLabel;
	        return Coveo.$$('i', {
	            className: iconCss,
	            title: hoverLabel,
	            'aria-hidden': true
	        }).el.outerHTML;
	    };
	    TableauHelper.customDate = function (d) {
	        var dateOnly = Coveo.DateUtils.keepOnlyDatePart(d);
	        var today = Coveo.DateUtils.keepOnlyDatePart(new Date());
	        var options = {};
	        if (dateOnly.getFullYear() === today.getFullYear()) {
	            options.predefinedFormat = 'MMM dd';
	        }
	        else {
	            options.predefinedFormat = 'MMM dd, yyyy';
	        }
	        return Coveo.DateUtils.dateToString(d, options);
	    };
	    TableauHelper.customDateInternal = function (d) {
	        var options = {};
	        options.predefinedFormat = 'MM/dd/yyyy';
	        return Coveo.DateUtils.dateToString(d, options);
	    };
	    TableauHelper.KBExternalLink = function (result, options) {
	        var kbExternalUrl = '';
	        var kbArticleType = result.raw.sfkbarticletype || result.raw.sfknowledgebasearticletype || '';
	        var lang = result.raw.sflanguage || '';
	        if (kbArticleType) {
	            kbArticleType = kbArticleType.replace(/__kav/i, '');
	            var kbUrlName = result.raw.sfkburlname || result.raw.sfknowledgebaseurlname || '';
	            var urlPath = kbArticleType + '/' + kbUrlName;
	            lang = lang.substring(0, 2) || lang;
	            urlPath = result.raw.commonlanguage.toLowerCase() == 'english' ? urlPath : (urlPath + '?lang=' + lang);
	            kbExternalUrl = kbUrlName ? (options.baseUrl + urlPath) : result.raw.clickUri;
	        }
	        return kbExternalUrl;
	    };
	    TableauHelper.isAPublicKB = function (result) {
	        var retVal = false;
	        if (result.raw.sfkbid && result.raw.sfkbisvisibleinpkb === 'True') {
	            retVal = true;
	        }
	        if (result.raw.sfkbid && result.raw.sfknowledgebaseisvisibleinpkb === 'True') {
	            retVal = true;
	        }
	        return retVal;
	    };
	    TableauHelper.isATwbx = function (result) {
	        var retVal = false;
	        if (result.title.indexOf('.twb') > -1) {
	            retVal = true;
	        }
	        return retVal;
	    };
	    TableauHelper.isCaseContextActivated = function () {
	        var retVal = false;
	        var caseContextEl = document.querySelector('.CoveoCaseContext');
	        if (caseContextEl) {
	            var caseContext = Coveo.Component.get(caseContextEl, CaseContext_1.CaseContext);
	            retVal = caseContext ? caseContext.isContextActivated() : false;
	        }
	        return retVal;
	    };
	    // Create the XHR object.
	    TableauHelper.createCORSRequest = function (method, url, enableCorsAnywhere) {
	        if (enableCorsAnywhere === void 0) { enableCorsAnywhere = false; }
	        var cors_api_url = enableCorsAnywhere ? 'https://cors-anywhere.herokuapp.com/' : '';
	        // var cors_api_url = '';
	        var xhr = new XMLHttpRequest();
	        if ('withCredentials' in xhr) {
	            // XHR for Chrome/Firefox/Opera/Safari.
	            xhr.open(method, cors_api_url + url, true);
	        }
	        else if (typeof XDomainRequest != 'undefined') {
	            // XDomainRequest for IE.
	            xhr = new XDomainRequest();
	            xhr.open(method, cors_api_url + url);
	        }
	        else {
	            // CORS not supported.
	            xhr = null;
	        }
	        return xhr;
	    };
	    TableauHelper.kbExternalLinkOptions = {
	        baseUrl: 'http://kb.tableau.com/articles/'
	    };
	    return TableauHelper;
	}());
	exports.TableauHelper = TableauHelper;


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var CustomEvents_1 = __webpack_require__(2);
	var Component = Coveo.Component;
	var Initialization = Coveo.Initialization;
	var ComponentOptions = Coveo.ComponentOptions;
	/**
	 * Component used to render Case details information when the case context is available.
	 */
	var CaseContext = (function (_super) {
	    __extends(CaseContext, _super);
	    function CaseContext(element, options, bindings) {
	        var _this = this;
	        _super.call(this, element, CaseContext.ID, bindings);
	        this.element = element;
	        this.options = options;
	        this.bindings = bindings;
	        this.defaultSuggestionTemplate = _.template('<div><%-raw.sfcasenumber%></div><div><%-title%></div>');
	        this.options = ComponentOptions.initComponentOptions(element, CaseContext, options);
	        this.caseID = Coveo.Utils.getQueryStringValue('caseid');
	        // this.queryStateModel.registerNewAttribute('caseId', '');
	        // this.firstLoading = true;
	        if (this.options.suggestionTemplate == null) {
	            this.options.suggestionTemplate = new Coveo.Template(this.defaultSuggestionTemplate);
	        }
	        this.bind.onRootElement(Coveo.QueryEvents.querySuccess, function (args) { return _this.handleQuerySuccess(args); });
	        // this.bind.onRootElement('state:change:caseId', (args: Coveo.IAttributeChangedEventArg) => { this.renderComponent() });
	        this.renderComponent();
	    }
	    CaseContext.prototype.isContextActivated = function () {
	        var hd = Coveo.HashUtils.getValue('hd', Coveo.HashUtils.getHash());
	        return (!Coveo.Utils.isNullOrEmptyString(this.caseID) && !Coveo.Utils.isNullOrEmptyString(hd));
	        // return !Coveo.Utils.isNullOrEmptyString(this.caseID) ||
	        //   (this.firstLoading && !Coveo.Utils.isNullOrEmptyString(Coveo.Utils.getQueryStringValue('caseId')));
	    };
	    CaseContext.prototype.renderComponent = function () {
	        // to support box expand link as well...
	        // if (this.firstLoading && Coveo.Utils.getQueryStringValue('caseId') && !this.queryStateModel.get('caseId')) {
	        //   this.queryStateModel.set('caseId', Coveo.Utils.getQueryStringValue('caseId'));
	        //   this.firstLoading = false;
	        // }
	        // this.caseID = this.queryStateModel.get('caseId');
	        // console.log(this.caseID);
	        var _this = this;
	        if (this.isContextActivated()) {
	            // we must change the attachToCaseEndpoint each time this component re-render.
	            // if (window['attachToCaseEndpoint']) {
	            //   window['attachToCaseEndpoint'].caseId = this.caseID;
	            //   console.log('attachToCaseEndpoint has been updated: ' + this.caseID);
	            // }
	            // Coveo.$$(this.element).addClass('coveo-fade-out');
	            // this.getCaseResult = () => {
	            //   return new Promise<Coveo.IQueryResult>((resolve, reject) => {
	            //     Coveo.SearchEndpoint.endpoints['default'].search({
	            //       q: '@sfcaseid==' + this.caseID,
	            //       cq: '@objecttype==Case',
	            //       numberOfResults: 1
	            //     }).done((data) => { resolve(_.first(data.results)); });
	            //   });
	            // };
	            this.gatheringContext = function () { return _this.handleGatheringContext(); };
	            // let caseResult = this.getCaseResult();
	            // let caseResult = () => undefined;
	            // let promise = new Promise<Coveo.IQueryResult>((resolve, reject) => {
	            //   resolve(caseResult);
	            // }).then((result) => {
	            this.element.innerHTML = '';
	            // this.caseResult = result;
	            // this.options.caseNumber = this.caseResult ? this.caseResult.raw.sfcasenumber : this.options.caseNumber;
	            this.element.appendChild(this.buildContextHeader());
	            this.element.appendChild(this.buildContextContent());
	            Coveo.$$(this.element).removeClass('coveo-fade-out');
	        }
	        else {
	            this.element.innerHTML = '';
	            this.element.appendChild(this.buildContextFinderHeader());
	            this.element.appendChild(this.buildContextFinderBox());
	        }
	    };
	    CaseContext.prototype.buildContextFinderHeader = function () {
	        var headerElement = Coveo.$$('div', { className: 'coveo-facet-header' }).el;
	        var titleElement = Coveo.$$('div', { className: 'coveo-facet-header-title' }, Coveo.l('SmartCaseFinder')).el;
	        headerElement.appendChild(titleElement);
	        return headerElement;
	    };
	    CaseContext.prototype.buildContextFinderBox = function () {
	        var _this = this;
	        var finderElement = Coveo.$$('div', { className: 'coveo-context-finder' }).el;
	        var finderBox = Coveo.$$('div', { className: 'coveo-context-finder-box' }).el;
	        this.magicBox = Coveo.MagicBox.create(finderBox, Coveo.MagicBox.Grammars.ExpressionsGrammar(Coveo.MagicBox.Grammars.Complete), {
	            inline: this.options.inline,
	            suggestionTimeout: this.options.suggestionTimeout
	        });
	        if (this.options.placeholder) {
	            this.magicBox.element.querySelector('input').placeholder = Coveo.l(this.options.placeholder);
	        }
	        this.magicBox.getSuggestions = function () { return _this.handleSuggestions(); };
	        finderElement.appendChild(finderBox);
	        return finderElement;
	    };
	    CaseContext.prototype.buildContextHeader = function () {
	        var contextHeaderElement = Coveo.$$('div', { className: 'coveo-facet-header' }).el;
	        var eraserElement = this.buildEraser();
	        var titleElement = Coveo.$$('a', {
	            className: 'coveo-facet-header-title',
	            href: this.caseResult ? this.caseResult.clickUri : this.options.caseUri,
	            target: '_blank'
	        }, 'Case: ' + this.options.caseNumber).el;
	        contextHeaderElement.appendChild(eraserElement);
	        contextHeaderElement.appendChild(titleElement);
	        return contextHeaderElement;
	    };
	    CaseContext.prototype.buildEraser = function () {
	        var _this = this;
	        // let icon = Coveo.$$('span', { className: 'coveo-icon' });
	        var icon = Coveo.$$('span', { className: 'icon-clear' });
	        var eraser = Coveo.$$('div', {
	            className: 'coveo-facet-header-eraser',
	            title: Coveo.l('Clear', this.options.title)
	        });
	        eraser.append(icon.el);
	        eraser.on('click', function () {
	            var hq = Coveo.Component.get(document.querySelector('.CoveoHiddenQuery'));
	            Coveo.state(_this.root, 'caseId', '');
	            if (hq != null) {
	                hq.clear();
	            }
	            else {
	                Coveo.executeQuery(_this.root);
	            }
	        });
	        return eraser.el;
	    };
	    CaseContext.prototype.buildContextContent = function () {
	        var _this = this;
	        var contextContentElement = Coveo.$$('ul', { className: 'coveo-context-content' }).el;
	        var ctxtInfo = this.gatheringContext();
	        // ctxtInfo
	        var promise = new Promise(function (resolve, reject) {
	            resolve(ctxtInfo);
	        }).then(function (context) {
	            _.each(context, function (val, key) {
	                var normalizedField = key.replace(/[\s\/]/g, '-');
	                var contextSection = Coveo.$$('li', { className: 'coveo-context-section coveo-context-' + normalizedField });
	                contextSection.append(Coveo.$$('div', { className: 'coveo-context-title' }, key).el);
	                contextSection.append(Coveo.$$('div', { className: 'coveo-context-info' }, val).el);
	                contextContentElement.appendChild(contextSection.el);
	            });
	            contextContentElement.appendChild(Coveo.$$('li', { className: 'coveo-context-section' }, _this.buildContextFinderBox()).el);
	        });
	        return contextContentElement;
	    };
	    CaseContext.prototype.handleQuerySuccess = function (data) {
	        this.renderComponent();
	    };
	    CaseContext.prototype.handleSuggestions = function () {
	        var suggestionsEventArgs = {
	            suggestions: [],
	            caseContext: this
	        };
	        this.bind.trigger(this.element, CustomEvents_1.CustomEvents.populateContextBoxSuggestions, suggestionsEventArgs);
	        return suggestionsEventArgs.suggestions;
	    };
	    CaseContext.prototype.handleGatheringContext = function () {
	        var gatheringContextEventArgs = {
	            context: this.options.contextInfo,
	            caseResult: this.caseResult,
	            caseId: this.caseID,
	            caseNumber: this.options.caseNumber,
	            caseUri: this.options.caseUri
	        };
	        this.bind.trigger(this.element, CustomEvents_1.CustomEvents.gatheringContext, gatheringContextEventArgs);
	        return gatheringContextEventArgs.context;
	    };
	    CaseContext.ID = 'CaseContext';
	    /**
	    * The options for the component
	    * @componentOptions
	    */
	    CaseContext.options = {
	        title: Coveo.ComponentOptions.buildStringOption({ required: true, defaultValue: 'Case Context' }),
	        caseNumber: Coveo.ComponentOptions.buildStringOption({ required: true }),
	        caseUri: Coveo.ComponentOptions.buildStringOption(),
	        placeholder: Coveo.ComponentOptions.buildStringOption({ defaultValue: 'SelectYourCase' }),
	        inline: Coveo.ComponentOptions.buildBooleanOption({ defaultValue: false }),
	        suggestionTemplate: Coveo.ComponentOptions.buildTemplateOption({
	            selectorAttr: 'data-template-selector',
	            idAttr: 'data-template-id'
	        }),
	        /**
	        * Specifies a timeout before rejecting suggestions in the caseContex search.<br/>
	        * Default value is 500 (0.5 second).
	        */
	        suggestionTimeout: ComponentOptions.buildNumberOption({ defaultValue: 500 }),
	        suggestionConstantExpression: Coveo.ComponentOptions.buildStringOption({ defaultValue: '@objecttype==Case' }),
	        contextInfo: ComponentOptions.buildCustomOption(function () {
	            return null;
	        })
	    };
	    return CaseContext;
	}(Component));
	exports.CaseContext = CaseContext;
	Initialization.registerAutoCreateComponent(CaseContext);


/***/ }),
/* 7 */
/***/ (function(module, exports) {

	"use strict";
	var PRODUCT_FACET_ID = 'tableau-facet-1';
	var LANGUAGE_FACET_ID = 'tableau-facet-2';
	var SOURCE_FACET_ID = 'tableau-facet-3';
	var VERSION_FACET_ID = 'tableau-facet-4';
	var TableauDependentFacetManager = (function () {
	    function TableauDependentFacetManager() {
	    }
	    TableauDependentFacetManager.handleDependentFacets = function () {
	        var dependentFacets = document.getElementsByClassName('CoveoFacet custom-dependentFacet');
	        _.each(dependentFacets, function (facet) {
	            var id = facet.getAttribute('id');
	            TableauDependentFacetManager.showOrHideFacet(id);
	        });
	    };
	    TableauDependentFacetManager.showOrHideFacet = function (facetid) {
	        var facetDom = Coveo.$$(document.getElementById(facetid));
	        var facetComponent = Coveo.Component.get(facetDom.el);
	        if (TableauDependentFacetManager.shouldDisplayFacet(facetid)) {
	            facetComponent.enable();
	            facetDom.removeClass('custom-disable');
	        }
	        else {
	            facetComponent.disable();
	            facetDom.addClass('custom-disable');
	        }
	    };
	    TableauDependentFacetManager.shouldDisplayFacet = function (facetid) {
	        var retVal = false;
	        switch (facetid) {
	            case PRODUCT_FACET_ID:
	                retVal = true;
	                break;
	            case LANGUAGE_FACET_ID:
	                retVal = true;
	                break;
	            case SOURCE_FACET_ID:
	                retVal = true;
	                break;
	            case VERSION_FACET_ID:
	                retVal = true;
	                break;
	        }
	        return retVal;
	    };
	    TableauDependentFacetManager.isFacetSelected = function (facetid) {
	        var retVal = false;
	        var facetElem = document.getElementById(facetid);
	        if (facetElem) {
	            var facetComponent = Coveo.Component.get(facetElem);
	            retVal = facetComponent.getSelectedValues().length || facetComponent.getExcludedValues().length ? true : false;
	        }
	        return retVal;
	    };
	    TableauDependentFacetManager.disallowedValuesOnFacetResults = function (groupByResults, facetField, values) {
	        var targetFacet = _.findWhere(groupByResults, { field: facetField });
	        return _.reject(targetFacet.values, function (v) {
	            return _.contains(values, v.value.toLowerCase());
	        });
	    };
	    TableauDependentFacetManager.isFacetValuesSelected = function (facetid, valuesToCheck, exactMatchMode) {
	        if (exactMatchMode === void 0) { exactMatchMode = false; }
	        var retVal = false;
	        var facetElem = document.getElementById(facetid);
	        if (facetElem) {
	            var facetComponent = Coveo.Component.get(facetElem);
	            var selectedValues = facetComponent.getSelectedValues();
	            var excludedValues = facetComponent.getExcludedValues();
	            var displayedValues = facetComponent.getDisplayedValues();
	            selectedValues = _.map(selectedValues, function (v) { return v.toLowerCase(); });
	            excludedValues = _.map(excludedValues, function (v) { return v.toLowerCase(); });
	            displayedValues = _.map(displayedValues, function (v) { return v.toLowerCase(); });
	            valuesToCheck = _.map(valuesToCheck, function (v) { return v.toLowerCase(); });
	            retVal = exactMatchMode ?
	                ((selectedValues.length === valuesToCheck.length && _.difference(selectedValues, valuesToCheck).length === 0) ||
	                    (displayedValues.length === valuesToCheck.length && _.difference(displayedValues, valuesToCheck).length === 0) ? true : false) :
	                (_.intersection(selectedValues, valuesToCheck).length || _.intersection(excludedValues, valuesToCheck).length ? true : false);
	        }
	        return retVal;
	    };
	    TableauDependentFacetManager.isFacetValueSelectedContains = function (facetid, value, mustHaveOnlyOneMode) {
	        if (mustHaveOnlyOneMode === void 0) { mustHaveOnlyOneMode = false; }
	        var retVal = false;
	        var facetElem = document.getElementById(facetid);
	        if (facetElem) {
	            var facetComponent = Coveo.Component.get(facetElem);
	            var selectedValues = facetComponent.getSelectedValues();
	            selectedValues = _.map(selectedValues, function (v) { return v.toLowerCase(); });
	            value = value.toLowerCase();
	            retVal = mustHaveOnlyOneMode ? (selectedValues.length === 1 && selectedValues[0].indexOf(value) >= 0) :
	                _.any(selectedValues, function (v) { return v.indexOf(value) >= 0; });
	        }
	        return retVal;
	    };
	    return TableauDependentFacetManager;
	}());
	exports.TableauDependentFacetManager = TableauDependentFacetManager;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var $$ = Coveo.$$;
	// import {
	//   CustomEvents,
	//   ICustomSendEmailEventArgs,
	//   IPopulateContextBoxSuggestionsEventArgs,
	//   IGatheringContextEventArgs
	// } from '../events/CustomEvents';
	var TableauHelper_1 = __webpack_require__(5);
	/**
	 * Required customization specifically applied for Tableau's implementation
	 */
	var TableauCustoInternal = (function () {
	    function TableauCustoInternal(searchInterfaceElement) {
	        var _this = this;
	        this.searchInterfaceElement = searchInterfaceElement;
	        this.rootElement = $$(searchInterfaceElement);
	        var changeAllFacetsEvtNames = _.map(this.rootElement.findAll('.CoveoFacet'), function (el) {
	            return _this.getStateEventName(Coveo.QueryStateModel.eventTypes.changeOne + 'f:' + el.dataset['id']);
	        });
	        // Initialization Events
	        this.rootElement.on(Coveo.InitializationEvents.afterInitialization, function () { return _this.handleAfterInit(); });
	        this.rootElement.on(Coveo.InitializationEvents.afterComponentsInitialization, function () { return _this.handleAfterComponentsInit(); });
	        // Query Events
	        this.rootElement.on(Coveo.QueryEvents.newQuery, function (e, data) { return _this.handleNewQuery(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.buildingQuery, function (e, data) { return _this.handleBuildingQuery(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.doneBuildingQuery, function (e, data) { return _this.handleDoneBuildingQuery(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.preprocessResults, function (e, data) { return _this.handlePreprocessResults(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.querySuccess, function (e, data) { return _this.handleQuerySuccess(e, data); });
	        // State Events    
	        this.rootElement.on(changeAllFacetsEvtNames, function (e, data) { return _this.handleAllFacetsStateChanged(e, data); });
	        // Custom Events
	    }
	    TableauCustoInternal.prototype.getStateEventName = function (event) {
	        return Coveo.QueryStateModel.ID + ':' + event;
	    };
	    /**
	     * After Initialization
	     * initializing custom strings during before init event to avoid any issue with SFDC init strings.
	     */
	    TableauCustoInternal.prototype.handleAfterInit = function () {
	        var _this = this;
	        this.initStrings();
	        this.rootElement.one(Coveo.QueryEvents.preprocessResults, function (evt, args) {
	            try {
	                // getting user Identities returned by REST API - Debug info must be ENABLED
	                if (args.results.userIdentities.length >= 2) {
	                    var userIdentity = args.results.userIdentities[1].name;
	                    //handle on the usage analytics client instance of this search interface
	                    //change the user name of the analytic
	                    _this.ua.client['userId'] = userIdentity;
	                }
	            }
	            catch (e) {
	                console.log('ERROR while preprocessing Results to set UA userId: ' + e.message);
	            }
	        });
	        // launch a query to trigger OneLogin authentication since auto-trigger-query is now disabled
	        Coveo.executeQuery(this.rootElement.el);
	        var ddButton = document.querySelector('.dropdown-button');
	        var ddMenu = document.querySelector('.dropdown-menu');
	        if (ddButton) {
	            Coveo.$$(ddButton).on('click', function (e) { return Coveo.$$(ddMenu).toggleClass('open'); });
	        }
	        // executing a query to set custom sorting order for the Alpo Created by facet. 
	        var query = {
	            field: "@alpoownername",
	            sortCriteria: "occurrences",
	            maximumNumberOfValues: 50,
	            queryOverride: "@uri"
	        };
	        var customSortOwner = [];
	        Coveo.SearchEndpoint.endpoints["default"].listFieldValues(query).done(function (fieldValues) {
	            _.each(fieldValues, function (val) {
	                customSortOwner.push(val.value);
	            });
	            var facetEl = document.querySelector('.CoveoFacet[data-id="allAlpoOwnerName"]');
	            var facetCmp = Coveo.Component.get(facetEl, Coveo.Facet);
	            facetCmp.options.customSort = customSortOwner;
	        });
	    };
	    /**
	     * After Component Initialization
	     * Adding Setting Menu items for Tableau training and Give Feedback links
	     * Registering custom template helper to manage tableau custom icons. see result templates
	     */
	    TableauCustoInternal.prototype.handleAfterComponentsInit = function () {
	        var _this = this;
	        this.searchInterface = Coveo.Component.get(this.rootElement.el, Coveo.SearchInterface);
	        this.ua = Coveo.Component.get(this.rootElement.find('.CoveoAnalytics'), Coveo.Analytics);
	        this.rootElement.on(Coveo.SettingsEvents.settingsPopulateMenu, function (e, args) {
	            // args.menuData.push({
	            //   className: 'coveo-give-feedback',
	            //   text: Coveo.l('TableauFeedback'),
	            //   onOpen: () => { window.open('https://mytableau.tableaucorp.com/x/IsqjBg', '_blank'); },
	            //   onClose: () => { }
	            // });
	            args.menuData.push({
	                className: 'tableau-training-link',
	                text: Coveo.l('TableauTrainingDoc'),
	                onOpen: function () { window.open('https://mytableau.tableaucorp.com/x/v86RBg', '_blank'); },
	                onClose: function () { }
	            });
	        });
	        Coveo.TemplateHelpers.registerTemplateHelper('fromTableauTypeToIcon', function (result, options) {
	            return TableauHelper_1.TableauHelper.fromTableauTypeToIcon(result, options);
	        });
	        Coveo.TemplateHelpers.registerTemplateHelper('fromTableauFileTypeToIcon', function (result, options) {
	            return TableauHelper_1.TableauHelper.fromTableauFileTypeToIcon(result, options);
	        });
	        Coveo.TemplateHelpers.registerTemplateHelper('fromTableauFileTypeToIconInternal', function (result, options) {
	            return TableauHelper_1.TableauHelper.fromTableauFileTypeToIconInternal(result, options);
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('customDate', function (value, options) {
	            return TableauHelper_1.TableauHelper.customDate(Coveo.DateUtils.convertFromJsonDateIfNeeded(value));
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('customDateInternal', function (value, options) {
	            return TableauHelper_1.TableauHelper.customDateInternal(Coveo.DateUtils.convertFromJsonDateIfNeeded(value));
	        });
	        this.rootElement.on(Coveo.QueryEvents.deferredQuerySuccess, function (evt, args) {
	            console.log('deferredQuerySuccess');
	            //disabling home-mode and enabling search as you type after the first query...
	            console.log('disabling home mode');
	            if (Coveo.$$(document.body).hasClass('tableau-home-mode') && !args.queryBuilder.expression.isEmpty()) {
	                Coveo.$$(document.body).removeClass('tableau-home-mode');
	                var searchBoxEl = document.querySelector('.CoveoSearchbox');
	                var searchBoxCmp = Coveo.get(searchBoxEl);
	                searchBoxCmp.options.enableSearchAsYouType = true;
	            }
	            console.log('removing loading screen');
	            var loadingEl = document.querySelector('.coveo-loading-screen');
	            if (loadingEl && !Coveo.$$(document.body).hasClass('tableau-home-mode')) {
	                Coveo.$$(loadingEl).remove();
	                _this.rootElement.removeClass('tableau-first-loading');
	            }
	            console.log('changing facet title');
	            var unspecifiedList = document.querySelectorAll('.coveo-facet-value-caption[title="Unspecified"]');
	            Array.prototype.forEach.call(unspecifiedList, function (el) {
	                el.title = Coveo.l('Unspecified');
	                el.className += ' unspecified';
	            });
	            //Customize error message for InvalidSyntax -- TO DO: Create a custom InvalidSyntax component
	            var querySummaryEl = document.querySelector('.coveo-summary-section');
	            var errorMoreInfoEl = document.querySelector('.coveo-error-more-info');
	            var magicBoxErrorEl = document.querySelector('.magic-box-error');
	            if (errorMoreInfoEl) {
	                errorMoreInfoEl.remove();
	            }
	            if ((args.results.exception && args.results.exception.code == 'InvalidSyntax') || (args.results.totalCount == 0 && magicBoxErrorEl.innerText != "")) {
	                var errorMoreInfoEl_1 = Coveo.$$('div', { className: 'coveo-error-more-info' }).el;
	                var linkEl = Coveo.$$('a', {
	                    href: 'https://onlinehelp.coveo.com/en/cloud/coveo_cloud_query_syntax_reference.htm',
	                    target: '_blank'
	                }, 'Coveo Syntax Reference').el;
	                var linkLabelEl = Coveo.$$('span', {}, Coveo.l('QueryExceptionInvalidSyntaxLink')).el;
	                errorMoreInfoEl_1.appendChild(linkLabelEl);
	                errorMoreInfoEl_1.appendChild(linkEl);
	                querySummaryEl.appendChild(errorMoreInfoEl_1);
	            }
	        });
	    };
	    /**
	     * New Query
	     */
	    TableauCustoInternal.prototype.handleNewQuery = function (evt, args) {
	        var q = this.searchInterface.queryStateModel.get('q');
	        var txt = q ? ' for "' + q + '"' : '';
	        var qryEntryEl = $$(this.searchInterfaceElement).find('.query-entry-placeholder');
	        if (qryEntryEl) {
	            qryEntryEl.innerText = txt;
	        }
	        // TableauDependentFacetManager.handleDependentFacets();
	    };
	    /**
	     * Building Query
	     */
	    TableauCustoInternal.prototype.handleBuildingQuery = function (evt, args) {
	        //var searchIntCmp = <Coveo.SearchInterface>Coveo.Component.get(<HTMLElement>document.getElementById('search'), Coveo.SearchInterface);
	        //args.queryBuilder.constantExpression.add(searchIntCmp.options.expression);
	        args.queryBuilder.enableDebug = Coveo.Utils.isEmptyString(this.ua.client['userId']);
	    };
	    /**;
	     * Done Building Query
	     */
	    TableauCustoInternal.prototype.handleDoneBuildingQuery = function (evt, args) { };
	    /**
	     * Preprocess Results
	     */
	    TableauCustoInternal.prototype.handlePreprocessResults = function (evt, args) {
	        _.each(args.results.results, function (result) {
	            // Add web =1 parameter for SharePoint content (http://www.evokeit.com/making-a-link-to-a-document-in-sharepoint-2013-open-in-browser/)
	            if (result.raw.commonsource == 'SharePoint' && result.raw.sysfiletype != 'spdocumentlibrarylist' && result.raw.sysfiletype != 'spsite' && result.raw.owsid) {
	                result.clickUri = result.clickUri + '?web=1';
	            }
	        });
	    };
	    /**
	     * Query Success
	     */
	    TableauCustoInternal.prototype.handleQuerySuccess = function (evt, args) { };
	    /**
	     * Any facets state changed
	     */
	    TableauCustoInternal.prototype.handleAllFacetsStateChanged = function (evt, args) {
	        var selectedValues = args.value;
	        var id = args.attribute.replace('f:', '');
	        if (selectedValues.length) {
	            var facetEl = document.querySelector('.CoveoFacet[data-id="' + id + '"]');
	            var facetCmp = Coveo.Component.get(facetEl, Coveo.Facet);
	            var previousValues = Coveo.HashUtils.getValue(args.attribute, Coveo.HashUtils.getHash()) || [];
	            if (facetCmp.getDisplayedValues().indexOf('Not Specified') >= 0 && previousValues.indexOf('Not Specified') < 0) {
	                facetCmp.selectValue('Not Specified');
	            }
	        }
	    };
	    /**
	     * Initialized Custom Strings for Tableau
	     */
	    TableauCustoInternal.prototype.initStrings = function () {
	        // Custom variable for current application
	        String.toLocaleString({
	            'en': {
	                'ShowingResultsOf': '{2} result<pl>s</pl>',
	                'TableauFeedback': 'Give Feedback',
	                'TableauTrainingDoc': 'FAQ',
	                'SelectYourCase': 'Search Cases...',
	                'SmartCaseFinder': 'Attach to case',
	                'NoRelatedItemsFound': 'No Related {0} found',
	                'NoRelatedCasesFound': 'No Support cases found',
	                'RelatedSupportCases': 'Support Cases',
	                'RelatedKBs': 'External Links',
	                'TranslatedLinks': 'Translated Links',
	                'OopsError': 'Sorry, something went wrong',
	                'ProblemPersists': '',
	                'Clear': 'Clear All',
	                'Unspecified': 'No labels associated with this category',
	                'QueryExceptionInvalidSyntaxLink': 'Please refer to: '
	            }
	        });
	    };
	    /**
	     * Set default options of different UI Components for Tableau.
	     */
	    TableauCustoInternal.prototype.getDefaultOptions = function () {
	        var now = new Date();
	        var myRanges = [
	            {
	                start: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 2),
	                end: new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1),
	                label: '24 hours',
	                endInclusive: false
	            },
	            {
	                start: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 8),
	                end: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 2),
	                label: 'Week',
	                endInclusive: false
	            },
	            {
	                start: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 31),
	                end: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 8),
	                label: 'Month',
	                endInclusive: false
	            },
	            {
	                start: new Date(now.getFullYear() - 1, now.getMonth(), now.getDate()),
	                end: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 31),
	                label: 'Year',
	                endInclusive: false
	            },
	            {
	                start: new Date(1900, 1, 1),
	                end: new Date(now.getFullYear() - 1, now.getMonth(), now.getDate()),
	                label: 'Two+ years',
	                endInclusive: false
	            }];
	        var tableauOptions = {
	            SalesforceResultLink: {
	                alwaysOpenInNewWindow: true
	            },
	            ResultLink: {
	                alwaysOpenInNewWindow: true
	            },
	            Facet: {
	                availableSorts: ['occurrences', 'score', 'alphaAscending', 'alphaDescending'],
	                valueCaption: TableauHelper_1.TableauHelper.getFacetValueCaptionInternal()
	            },
	            FacetRange: {
	                ranges: myRanges,
	                availableSorts: ['custom'],
	                customSort: ['24 hours', 'Week', 'Month', 'Year', '"Two+ years'],
	                enableMoreLess: false
	            },
	            allDocType: {
	                allowedValues: TableauHelper_1.TableauHelper.getTypeAllowedValues()
	            },
	            archiveDocType: {
	                allowedValues: TableauHelper_1.TableauHelper.getTypeAllowedValues()
	            }
	        };
	        return tableauOptions;
	    };
	    return TableauCustoInternal;
	}());
	exports.TableauCustoInternal = TableauCustoInternal;
	;


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var TableauHelper_1 = __webpack_require__(5);
	var TableauTrustStatus = (function () {
	    function TableauTrustStatus() {
	    }
	    //,
	    //   'tableau-development': {
	    //    url: 'https://trust.tableau.com/status/tableau-development',
	    //    name: 'Development',
	    //    enableCorsAnywhere: true
	    //  },
	    //  'tableau-internal': {
	    //    url: 'https://trust.tableau.com/status/tableau-internal',
	    //    name: 'Internal',
	    //    enableCorsAnywhere: true
	    //  }
	    TableauTrustStatus.setupTrustPromises = function (trusts) {
	        var promises = [];
	        _.each(trusts, function (t) {
	            var promise = new Promise(function (resolve, reject) {
	                var xhr = TableauTrustStatus.trustInfos[t].xhr;
	                if (!xhr) {
	                    console.log('CORS not supported');
	                    return;
	                }
	                xhr.onload = function () {
	                    if (xhr.status === 200) {
	                        console.log(xhr.statusText);
	                        var re = /<h4[\s]+?class=\"current-status-label\">(.*)<\/h4>[\s]+?<img.*src="([^"]*)"/gi;
	                        var m = re.exec(xhr.responseText);
	                        if (m !== null) {
	                            TableauTrustStatus.trustInfos[t].status = m[1];
	                            TableauTrustStatus.trustInfos[t].img = m[2].indexOf('tableau.com') >= 0 ? m[2] : 'https://trust.tableau.com' + m[2];
	                        }
	                        resolve(TableauTrustStatus.trustInfos[t]);
	                    }
	                    else {
	                        console.log(xhr.status);
	                        reject(xhr.status);
	                    }
	                };
	                xhr.send();
	            });
	            promises.push(promise);
	        });
	        return promises;
	    };
	    TableauTrustStatus.updateStatus = function (data) {
	        var _this = this;
	        console.log(this.trustInfos);
	        var trusts = [];
	        if (data.param1 === 'all') {
	            _.each(_.keys(this.trustInfos), function (trustServer) {
	                _this.trustInfos[trustServer].xhr = TableauHelper_1.TableauHelper.createCORSRequest('GET', _this.trustInfos[trustServer].url, _this.trustInfos[trustServer].enableCorsAnywhere);
	                trusts.push(trustServer);
	            });
	        }
	        else if (this.trustInfos[data.param1]) {
	            this.trustInfos[data.param1].xhr = TableauHelper_1.TableauHelper.createCORSRequest('GET', this.trustInfos[data.param1].url, this.trustInfos[data.param1].enableCorsAnywhere);
	            trusts.push(data.param1);
	        }
	        else {
	            console.log('data.param1 >>> ' + data.param1 + ' is not supported');
	        }
	        var promises = this.setupTrustPromises(trusts);
	        Promise.all(promises).then(function (values) {
	            var templateObj = {};
	            // console.log(values);
	            _.each(values, function (val) {
	                templateObj['tableau-' + val.name.toLowerCase()] = val;
	            });
	            templateObj = data.param1 === 'all' ? templateObj : templateObj[data.param1];
	            console.log(templateObj);
	            console.log(_.keys(templateObj));
	            data.element.innerHTML = Coveo.TemplateCache['templates'][data.param1].dataToString({ data: templateObj });
	        });
	    };
	    TableauTrustStatus.trustInfos = {
	        'tableau-online': {
	            url: 'https://trust.tableau.com/status/tableau-online',
	            name: 'Online',
	            enableCorsAnywhere: true
	        },
	        'tableau-public': {
	            url: 'https://trust.tableau.com/status/tableau-public',
	            name: 'Public',
	            enableCorsAnywhere: true
	        }
	    };
	    return TableauTrustStatus;
	}());
	exports.TableauTrustStatus = TableauTrustStatus;


/***/ }),
/* 10 */
/***/ (function(module, exports) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var Component = Coveo.Component;
	var Initialization = Coveo.Initialization;
	var ComponentOptions = Coveo.ComponentOptions;
	/**
	 * Allows search users to provide feedback to a group of people in charge
	 * of collecting feedback/comments on search itself or simply on the content.
	 */
	var FeedbackLink = (function (_super) {
	    __extends(FeedbackLink, _super);
	    function FeedbackLink(element, options, bindings) {
	        var _this = this;
	        _super.call(this, element, FeedbackLink.ID, bindings);
	        this.element = element;
	        this.options = options;
	        this.bindings = bindings;
	        this.options = ComponentOptions.initComponentOptions(element, FeedbackLink, options);
	        this.bind.onRootElement(Coveo.SettingsEvents.settingsPopulateMenu, function (args) {
	            var _self = _this;
	            args.menuData.push({
	                className: 'coveo-give-feedback',
	                text: _self.options.text,
	                onOpen: function () { return _this.handleClick(); },
	                onClose: function () { }
	            });
	        });
	    }
	    FeedbackLink.prototype.handleClick = function () {
	        var _self = this;
	        var customEventCause = { name: 'sendFeedback', type: 'custom' };
	        _self.usageAnalytics.logCustomEvent(customEventCause, {}, _self.root);
	        window.location.href = 'mailto:' + _self.options.toAddress + '?subject=' + _self.options.subject;
	    };
	    FeedbackLink.ID = 'FeedbackLink';
	    /**
	     * The options for the component
	     * @componentOptions
	     */
	    FeedbackLink.options = {
	        subject: Coveo.ComponentOptions.buildStringOption({ defaultValue: 'Feedback' }),
	        text: Coveo.ComponentOptions.buildStringOption({ defaultValue: 'Give Feedback' }),
	        toAddress: ComponentOptions.buildStringOption({ defaultValue: '' })
	    };
	    return FeedbackLink;
	}(Component));
	exports.FeedbackLink = FeedbackLink;
	Initialization.registerAutoCreateComponent(FeedbackLink);


/***/ }),
/* 11 */
/***/ (function(module, exports) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var Initialization = Coveo.Initialization;
	var ComponentOptions = Coveo.ComponentOptions;
	var HomeTab = (function (_super) {
	    __extends(HomeTab, _super);
	    function HomeTab(element, options, bindings) {
	        _super.call(this, element, options, bindings);
	        this.element = element;
	        this.options = options;
	        this.bindings = bindings;
	        this.options = ComponentOptions.initComponentOptions(element, HomeTab, options);
	        Coveo.$$(this.element).hide();
	        this.queryStateModel.setNewDefault(Coveo.QueryStateModel.attributesEnum.t, this.options.id);
	    }
	    HomeTab.prototype.handleBuildingQuery = function (data) {
	        var q = this.queryStateModel.get(Coveo.QueryStateModel.attributesEnum.q);
	        if (!this.disabled && this.isSelected() && Coveo.Utils.isNonEmptyString(q) && !this['isFirstQuery']) {
	            this.queryStateModel.set(Coveo.QueryStateModel.attributesEnum.t, this.options.defaultSearchTab);
	            this['isFirstQuery'] = true;
	        }
	        else {
	            this['isFirstQuery'] = false;
	        }
	    };
	    HomeTab.ID = 'HomeTab';
	    HomeTab.options = {
	        defaultSearchTab: Coveo.ComponentOptions.buildStringOption({ required: true, defaultValue: 'All' })
	    };
	    return HomeTab;
	}(Coveo.Tab));
	exports.HomeTab = HomeTab;
	Initialization.registerAutoCreateComponent(HomeTab);


/***/ }),
/* 12 */
/***/ (function(module, exports) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var Component = Coveo.Component;
	var Initialization = Coveo.Initialization;
	var ComponentOptions = Coveo.ComponentOptions;
	/**
	 * This component is meant to be used inside a result template to display the URI or path to access a result.
	 */
	var CustomPrintableUri = (function (_super) {
	    __extends(CustomPrintableUri, _super);
	    /**
	     * Create a new CustomPrintableUri
	     * @param element
	     * @param options
	     * @param bindings
	     * @param result
	     */
	    function CustomPrintableUri(element, options, bindings, result) {
	        _super.call(this, element, CustomPrintableUri.ID, bindings);
	        this.element = element;
	        this.options = options;
	        this.result = result;
	        this.options = ComponentOptions.initComponentOptions(element, CustomPrintableUri, options);
	        var parentsXml = result.raw.parents;
	        if (parentsXml) {
	            this.renderParentsXml(element, parentsXml);
	        }
	        else {
	            this.renderUri(element, result);
	        }
	    }
	    CustomPrintableUri.prototype.renderParentsXml = function (element, parentsXml) {
	        var xmlDoc = Coveo.Utils.parseXml(parentsXml);
	        //let parents = xmlDoc.getElementsByTagName('parent');
	        var tokens = [];
	        var seperators = [];
	        //WORKAROUND : Building breadcrumb from printable URI instead of parent.
	        var parents = this.result.printableUri.split('/');
	        var baseUri = parents[0] + '//' + parents[2] + '/' + parents[3] + '/';
	        for (var i = 4; i < parents.length - 1; i++) {
	            if (i > 4) {
	                var seperator = this.buildSeperator();
	                seperators.push(seperator);
	                element.appendChild(seperator);
	            }
	            var parent_1 = parents[i];
	            baseUri = baseUri + parents[i] + '/';
	            var token = this.buildHtmlToken(parent_1, baseUri);
	            tokens.push(token);
	            element.appendChild(token);
	        }
	        //for (let i = 0; i < parents.length; i++) {
	        //  if (i > 0) {
	        //    let seperator = this.buildSeperator();
	        //    seperators.push(seperator);
	        //    element.appendChild(seperator);
	        //  }
	        //  let parent = <Element>parents.item(i);
	        //  let token = this.buildHtmlToken(parent.getAttribute('name'), parent.getAttribute('uri'));
	        //  debugger;
	        //  console.log('Parent Name: ' + parent.getAttribute('name'));
	        //  if (this.result.printableUri.toLowerCase().indexOf(parent.getAttribute('name').toLowerCase()) > -1 ) {
	        //    console.log('yes');
	        //    tokens.push(token);
	        //    element.appendChild(token);
	        //  } else {
	        //    console.log('no');
	        //  }
	        //}
	        if (tokens.length > 1) {
	            var ellipsis_1 = this.buildEllipsis();
	            element.insertBefore(ellipsis_1, seperators[0]);
	            var ellipsisSeperator = this.buildSeperator();
	            element.insertBefore(ellipsisSeperator, ellipsis_1);
	            var contentWidth = 0;
	            var tokensWidth = [];
	            for (var i = 0; i < tokens.length; i++) {
	                tokensWidth[i] = tokens[i].offsetWidth;
	                contentWidth += tokensWidth[i];
	            }
	            var seperatorWidth = seperators[0].offsetWidth;
	            var ellipsisWidth = ellipsis_1.offsetWidth;
	            var availableWidth = element.offsetWidth;
	            if (availableWidth <= contentWidth && !this.options.alwaysExpand) {
	                contentWidth += ellipsisWidth + seperatorWidth;
	                var hidden_1 = [];
	                var i = 1;
	                while (i < tokens.length && availableWidth <= contentWidth) {
	                    element.removeChild(tokens[i]);
	                    element.removeChild(seperators[i - 1]);
	                    if (i > 1) {
	                        hidden_1.push(seperators[i - 1]);
	                    }
	                    hidden_1.push(tokens[i]);
	                    contentWidth -= tokensWidth[i] + seperatorWidth;
	                    i++;
	                }
	                ellipsis_1.onclick = function () {
	                    for (var i_1 = 0; i_1 < hidden_1.length; i_1++) {
	                        element.insertBefore(hidden_1[i_1], ellipsis_1);
	                    }
	                    element.removeChild(ellipsis_1);
	                };
	            }
	            else {
	                element.removeChild(ellipsis_1);
	                element.removeChild(ellipsisSeperator);
	            }
	        }
	    };
	    CustomPrintableUri.prototype.renderUri = function (element, result) {
	        this.uri = result.clickUri;
	        var stringAndHoles;
	        if (result.printableUri.indexOf('\\') == -1) {
	            stringAndHoles = Coveo.StringAndHoles.shortenUri(result.printableUri, Coveo.$$(element).width() / 7);
	        }
	        else {
	            stringAndHoles = Coveo.StringAndHoles.shortenPath(result.printableUri, Coveo.$$(element).width() / 7);
	        }
	        var uri = Coveo.HighlightUtils.highlightString(stringAndHoles.value, result.printableUriHighlights, stringAndHoles.holes, 'coveo-highlight');
	        var link = Coveo.$$('a');
	        link.setAttribute('title', result.printableUri);
	        link.addClass('coveo-printable-uri');
	        link.setHtml(uri);
	        link.setAttribute('href', result.clickUri);
	        this.bindLogOpenDocument(link.el);
	        element.appendChild(link.el);
	    };
	    CustomPrintableUri.prototype.buildSeperator = function () {
	        var seperator = document.createElement('span');
	        seperator.innerText = ' > ';
	        seperator.className = 'coveo-printable-uri-separator';
	        return seperator;
	    };
	    CustomPrintableUri.prototype.buildEllipsis = function () {
	        var ellipsis = document.createElement('span');
	        ellipsis.innerText = '...';
	        ellipsis.className = 'coveo-printable-uri';
	        return ellipsis;
	    };
	    CustomPrintableUri.prototype.buildHtmlToken = function (name, uri) {
	        var modifiedName = name.charAt(0).toUpperCase() + name.slice(1);
	        var link = document.createElement('a');
	        this.bindLogOpenDocument(link);
	        link.href = uri;
	        if (this.options.openLinkInNewWindow) {
	            link.target = '_blank';
	        }
	        this.uri = uri;
	        link.className = 'coveo-printable-uri';
	        link.appendChild(document.createTextNode(modifiedName));
	        return link;
	    };
	    CustomPrintableUri.prototype.bindLogOpenDocument = function (link) {
	        var _this = this;
	        Coveo.$$(link).on(['mousedown', 'touchend'], function (e) {
	            var url = Coveo.$$(e.srcElement).getAttribute('href');
	            var title = Coveo.$$(e.srcElement).text();
	            _this.usageAnalytics.logClickEvent(Coveo.analyticsActionCauseList.documentOpen, {
	                documentURL: url,
	                documentTitle: title,
	                author: _this.result.raw.author
	            }, _this.result, _this.root);
	        });
	    };
	    CustomPrintableUri.ID = 'CustomPrintableUri';
	    CustomPrintableUri.options = {
	        alwaysExpand: ComponentOptions.buildBooleanOption({ defaultValue: false }),
	        openLinkInNewWindow: ComponentOptions.buildBooleanOption({ defaultValue: false })
	    };
	    CustomPrintableUri.fields = [
	        'parents'
	    ];
	    return CustomPrintableUri;
	}(Component));
	exports.CustomPrintableUri = CustomPrintableUri;
	Initialization.registerAutoCreateComponent(CustomPrintableUri);


/***/ })
/******/ ])
});
;
//# sourceMappingURL=Coveo.Tableau.Internal.js.map