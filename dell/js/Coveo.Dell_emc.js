(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["CoveoExtension"] = factory();
	else
		root["CoveoExtension"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/js/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	var _this = this;
	var CustomEvents_1 = __webpack_require__(2);
	exports.CustomEvents = CustomEvents_1.CustomEvents;
	__export(__webpack_require__(3));
	var Dell_emcHelper_1 = __webpack_require__(5);
	exports.Dell_emcHelper = Dell_emcHelper_1.Dell_emcHelper;
	var Dell_emcCusto_1 = __webpack_require__(4);
	exports.Dell_emcCusto = Dell_emcCusto_1.Dell_emcCusto;
	var AdvancedSearchPanel_1 = __webpack_require__(6);
	exports.AdvancedSearchPanel = AdvancedSearchPanel_1.AdvancedSearchPanel;
	var OmniboxCustomSuggestions_1 = __webpack_require__(7);
	exports.OmniboxCustomSuggestions = OmniboxCustomSuggestions_1.OmniboxCustomSuggestions;
	// Webpack output a library target with a temporary name.
	// This is to allow end user to put CoveoPSComponents.js before or after the main CoveoJsSearch.js, without breaking
	// This code swap the current module to the "real" Coveo variable.
	var swapVar = function () {
	    if (window['Coveo'] == undefined) {
	        window['Coveo'] = _this;
	    }
	    else {
	        _.each(_.keys(_this), function (k) {
	            window['Coveo'][k] = _this[k];
	        });
	    }
	};
	swapVar();


/***/ }),
/* 2 */
/***/ (function(module, exports) {

	"use strict";
	var CustomEvents = (function () {
	    function CustomEvents() {
	    }
	    CustomEvents.yourCustomEvent = 'yourCustomEvent';
	    return CustomEvents;
	}());
	exports.CustomEvents = CustomEvents;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var Dell_emcCusto_1 = __webpack_require__(4);
	function initDell_emcCusto(element, options) {
	    if (options === void 0) { options = {}; }
	    Coveo.Initialization.initializeFramework(element, options, function () {
	        var custo = new Dell_emcCusto_1.Dell_emcCusto(element);
	        // custo.initStrings();
	        var customOptions = _.extend(options, custo.getDefaultOptions());
	        Coveo.Initialization.initSearchInterface(element, customOptions);
	    });
	}
	exports.initDell_emcCusto = initDell_emcCusto;
	function initDell_emcAgentBoxCusto(element, options) {
	    if (options === void 0) { options = {}; }
	    Coveo.Initialization.initializeFramework(element, options, function () {
	        var custo = new Dell_emcCusto_1.Dell_emcCusto(element);
	        // custo.initStrings();
	        var customOptions = _.extend(options, custo.getDefaultOptions());
	        Coveo.Initialization.initBoxInterface(element, customOptions);
	    });
	}
	exports.initDell_emcAgentBoxCusto = initDell_emcAgentBoxCusto;
	Coveo.Initialization.registerNamedMethod('initDell_emcCusto', function (element, options) {
	    if (options === void 0) { options = {}; }
	    initDell_emcCusto(element, options);
	});
	Coveo.Initialization.registerNamedMethod('initDell_emcAgentBoxCusto', function (element, options) {
	    if (options === void 0) { options = {}; }
	    initDell_emcAgentBoxCusto(element, options);
	});


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var $$ = Coveo.$$;
	var Dell_emcHelper_1 = __webpack_require__(5);
	/**
	 * Required customization specifically applied for Tableau's implementation
	 */
	var Dell_emcCusto = (function () {
	    function Dell_emcCusto(searchInterfaceElement) {
	        var _this = this;
	        this.searchInterfaceElement = searchInterfaceElement;
	        this.rootElement = $$(searchInterfaceElement);
	        // Initialization Events
	        this.rootElement.on(Coveo.InitializationEvents.beforeInitialization, function () { return _this.handleBeforeInit(); });
	        this.rootElement.on(Coveo.InitializationEvents.afterComponentsInitialization, function () { return _this.handleAfterComponentsInit(); });
	        this.rootElement.on(Coveo.InitializationEvents.afterInitialization, function () { return _this.handleAfterInit(); });
	        // Query Events
	        this.rootElement.on(Coveo.QueryEvents.newQuery, function (e, data) { return _this.handleNewQuery(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.buildingQuery, function (e, data) { return _this.handleBuildingQuery(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.doneBuildingQuery, function (e, data) { return _this.handleDoneBuildingQuery(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.preprocessResults, function (e, data) { return _this.handlePreprocessResults(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.querySuccess, function (e, data) { return _this.handleQuerySuccess(e, data); });
	        // State Events
	        // Custom Events
	        // TODO
	    }
	    Dell_emcCusto.prototype.getStateEventName = function (event) {
	        return Coveo.QueryStateModel.ID + ':' + event;
	    };
	    /**
	     * Before Initialization
	     */
	    Dell_emcCusto.prototype.handleBeforeInit = function () { };
	    /**
	     * After Initialization
	     * initializing custom strings during before init event to avoid any issue with SFDC init strings.
	     */
	    Dell_emcCusto.prototype.handleAfterInit = function () {
	        this.initStrings();
	        //Select english facet by default
	        //let languageFacet = <Coveo.Facet>Coveo.Component.get(<HTMLElement>document.querySelector('[data-id="langFacet"]'), Coveo.Facet);
	        //languageFacet.selectValue('English');
	    };
	    /**
	     * After Component Initialization
	     * Adding Setting Menu item for Tableau training link
	     * Registering custom template helper to manage tableau custom icons. see result templates
	     */
	    Dell_emcCusto.prototype.handleAfterComponentsInit = function () {
	        this.searchInterface = Coveo.Component.get(this.rootElement.el, Coveo.SearchInterface);
	        var searchBox = document.querySelector('.CoveoSearchbox input');
	        //to-do: Move to String object
	        searchBox.setAttribute('placeholder', 'Search Support or Find Service Request by Number');
	        Coveo.TemplateHelpers.registerTemplateHelper('fromDell_emcTypeToIcon', function (result, options) {
	            return Dell_emcHelper_1.Dell_emcHelper.fromDell_emcTypeToIcon(result, options);
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('customDate', function (value, options) {
	            return Dell_emcHelper_1.Dell_emcHelper.customDate(Coveo.DateUtils.convertFromJsonDateIfNeeded(value));
	        });
	    };
	    /**
	     * New Query
	     */
	    Dell_emcCusto.prototype.handleNewQuery = function (evt, args) { };
	    /**
	     * Building Query
	     */
	    Dell_emcCusto.prototype.handleBuildingQuery = function (evt, args) { };
	    /**
	     * Done Building Query
	     */
	    Dell_emcCusto.prototype.handleDoneBuildingQuery = function (evt, args) { };
	    /**
	     * Preprocess Results
	     */
	    Dell_emcCusto.prototype.handlePreprocessResults = function (evt, args) { };
	    /**
	     * Query Success
	     */
	    Dell_emcCusto.prototype.handleQuerySuccess = function (evt, args) { };
	    /**
	     * Initialized Custom Strings for Tableau
	     */
	    Dell_emcCusto.prototype.initStrings = function () {
	        // Custom variable for current application
	        String.toLocaleString({
	            'en': {
	                'ShowingResultsOf': 'Result<pl>s</pl> {0}<pl>-{1}</pl> of about {2}',
	                'RemoveContext': 'Remove Case Filters',
	                'GoToFullSearch': 'Full Search Page'
	            }
	        });
	    };
	    /**
	     * Set default options of different UI Components for Dell_emc.
	     */
	    Dell_emcCusto.prototype.getDefaultOptions = function () {
	        var now = new Date();
	        var myRanges = [
	            {
	                start: new Date(1900, 1, 1),
	                end: new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1),
	                label: 'All Dates',
	                endInclusive: false
	            },
	            {
	                start: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1),
	                end: new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1),
	                label: 'Within Last Day',
	                endInclusive: false
	            },
	            {
	                start: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 7),
	                end: new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1),
	                label: 'Within Last Week',
	                endInclusive: false
	            },
	            {
	                start: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 31),
	                end: new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1),
	                label: 'Within Last Month',
	                endInclusive: false
	            },
	            {
	                start: new Date(now.getFullYear() - 1, now.getMonth(), now.getDate()),
	                end: new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1),
	                label: 'Within Last Year',
	                endInclusive: false
	            },
	        ];
	        var defaultOptions = {
	            SalesforceResultLink: {
	                alwaysOpenInNewWindow: true
	            },
	            ResultLink: {
	                alwaysOpenInNewWindow: true
	            },
	            Facet: {
	                availableSorts: ['occurrences', 'score', 'alphaAscending', 'alphaDescending']
	            },
	            FacetRange: {
	                ranges: myRanges,
	                availableSorts: ['custom'],
	                customSort: ['All Dates', 'Within Last Day', 'Within Last Week', 'Within Last Month', 'Within Last Year'],
	                enableMoreLess: false
	            }
	        };
	        return defaultOptions;
	    };
	    return Dell_emcCusto;
	}());
	exports.Dell_emcCusto = Dell_emcCusto;
	;


/***/ }),
/* 5 */
/***/ (function(module, exports) {

	"use strict";
	var SOURCE_ICONS_MAPPING = {
	    'unknown': 'dellemc-knowledge'
	};
	var IDell_emcIconOptions = (function () {
	    function IDell_emcIconOptions() {
	        this.value = null;
	        this.additionalClass = null;
	    }
	    return IDell_emcIconOptions;
	}());
	exports.IDell_emcIconOptions = IDell_emcIconOptions;
	var Dell_emcHelper = (function () {
	    function Dell_emcHelper() {
	    }
	    Dell_emcHelper.fromDell_emcTypeToIcon = function (result, options) {
	        var iconCss = options.value;
	        var hoverLabel = result.raw.commonsource || '';
	        if (Coveo.Utils.isNullOrEmptyString(iconCss)) {
	            var commonSrc = (result.raw.commonsource || '').toLowerCase();
	            iconCss = SOURCE_ICONS_MAPPING[commonSrc.toLowerCase()] || SOURCE_ICONS_MAPPING['unknown'];
	            iconCss = !Coveo.Utils.isNullOrEmptyString(options.additionalClass) ? (iconCss + ' ' + options.additionalClass) : iconCss;
	        }
	        return Coveo.$$('div', {
	            className: iconCss,
	            title: hoverLabel
	        }).el.outerHTML;
	    };
	    Dell_emcHelper.customDate = function (d) {
	        var dateOnly = Coveo.DateUtils.keepOnlyDatePart(d);
	        var today = Coveo.DateUtils.keepOnlyDatePart(new Date());
	        var options = {};
	        if (dateOnly.getFullYear() === today.getFullYear()) {
	            options.predefinedFormat = 'MMM dd';
	        }
	        else {
	            options.predefinedFormat = 'MMM dd, yyyy';
	        }
	        return Coveo.DateUtils.dateToString(d, options);
	    };
	    Dell_emcHelper.KBExternalLink = function (result, options) {
	        var kbExternalUrl = '';
	        var kbArticleType = result.raw.sfkbarticletype || result.raw.sfknowledgebasearticletype || '';
	        var lang = result.raw.sflanguage || '';
	        if (kbArticleType) {
	            kbArticleType = kbArticleType.replace(/__kav/i, '');
	            var kbUrlName = result.raw.sfkburlname || result.raw.sfknowledgebaseurlname || '';
	            var urlPath = kbArticleType + '/' + kbUrlName;
	            lang = lang.substring(0, 2) || lang;
	            urlPath = result.raw.commonlanguage.toLowerCase() == 'english' ? urlPath : (urlPath + '?lang=' + lang);
	            kbExternalUrl = kbUrlName ? (options.baseUrl + urlPath) : result.raw.clickUri;
	        }
	        return kbExternalUrl;
	    };
	    Dell_emcHelper.isAPublicKB = function (result) {
	        var retVal = false;
	        if (result.raw.sfkbid && result.raw.sfkbisvisibleinpkb === 'True') {
	            retVal = true;
	        }
	        if (result.raw.sfkbid && result.raw.sfknowledgebaseisvisibleinpkb === 'True') {
	            retVal = true;
	        }
	        return retVal;
	    };
	    Dell_emcHelper.kbExternalLinkOptions = {
	        baseUrl: 'http://kb.Dell_emc.com/articles/'
	    };
	    return Dell_emcHelper;
	}());
	exports.Dell_emcHelper = Dell_emcHelper;


/***/ }),
/* 6 */
/***/ (function(module, exports) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var Component = Coveo.Component;
	var Initialization = Coveo.Initialization;
	var ComponentOptions = Coveo.ComponentOptions;
	/**
	 * This component is meant to be used inside a result template to display the URI or path to access a result.
	 */
	var AdvancedSearchPanel = (function (_super) {
	    __extends(AdvancedSearchPanel, _super);
	    /**
	     * Create a new AdvancedSearchPanel
	     * @param element
	     * @param options
	     * @param bindings
	     * @param result
	     */
	    function AdvancedSearchPanel(element, options, bindings, result) {
	        var _this = this;
	        _super.call(this, element, AdvancedSearchPanel.ID, bindings);
	        this.element = element;
	        this.options = options;
	        this.result = result;
	        this.options = ComponentOptions.initComponentOptions(element, AdvancedSearchPanel, options);
	        this.bind.onRootElement(Coveo.QueryEvents.buildingQuery, function (args) { return _this.handleBuildingQuery(args); });
	        this.init();
	    }
	    AdvancedSearchPanel.prototype.init = function () {
	        this.element.appendChild(this.buildLink());
	        this.element.appendChild(this.buildPanel());
	    };
	    AdvancedSearchPanel.prototype.buildLink = function () {
	        var _this = this;
	        var link = Coveo.$$('div', {
	            className: 'coveo-advanced-search-link'
	        }, '\u25b6 Advanced Search');
	        link.on('click', function () {
	            _this.showHidePanel();
	            if (link.text() == '\u25b6 Advanced Search') {
	                link.text('\u25bc Advanced Search');
	            }
	            else {
	                link.text('\u25b6 Advanced Search');
	            }
	        });
	        return link.el;
	    };
	    AdvancedSearchPanel.prototype.handleBuildingQuery = function (data) {
	        //handle exact match
	        var exactKeywords = document.querySelector('.coveo-advanced-search-panel-exact-input');
	        if (exactKeywords.value) {
	            data.queryBuilder.advancedExpression.add('"' + exactKeywords.value.replace('"', '') + '"');
	        }
	        //handle any keywords
	        var anyKeywords = document.querySelector('.coveo-advanced-search-panel-any-input');
	        if (anyKeywords.value) {
	            data.queryBuilder.advancedExpression.add(anyKeywords.value.split(' ').join(' OR '));
	        }
	        //handle any keywords
	        var allKeywords = document.querySelector('.coveo-advanced-search-panel-all-input');
	        if (allKeywords.value) {
	            data.queryBuilder.advancedExpression.add(allKeywords.value);
	        }
	        //handle none
	        var noneKeywords = document.querySelector('.coveo-advanced-search-panel-none-input');
	        if (noneKeywords.value) {
	            var keywords = noneKeywords.value.split(' ');
	            var exp = '';
	            _.each(keywords, function (k) {
	                if (k) {
	                    exp = exp + ' -' + k;
	                }
	            });
	            data.queryBuilder.advancedExpression.add(exp);
	        }
	        //handle search within
	        var searchWithinRadio = document.querySelector('.coveo-advanced-search-panel-search-within-title');
	        if (searchWithinRadio.checked) {
	            var queryBox = document.querySelector('.CoveoQuerybox');
	            data.queryBuilder.advancedExpression.addFieldExpression('@systitle', '=', queryBox.textContent.split(' '));
	            data.queryBuilder.expression.remove(queryBox.textContent);
	        }
	    };
	    AdvancedSearchPanel.prototype.buildPanel = function () {
	        var panel = Coveo.$$('div', {
	            id: 'coveo-advanced-search-panel',
	            className: 'coveo-advanced-search-panel'
	        });
	        var row1 = Coveo.$$('div', { id: 'row' });
	        var row2 = Coveo.$$('div', { id: 'row' });
	        var row3 = Coveo.$$('div', { id: 'row' });
	        row1.el.appendChild(this.buildAllKeywordsSection());
	        row1.el.appendChild(this.buildExactPhraseSection());
	        row2.el.appendChild(this.buildAnyKeywordsSection());
	        row2.el.appendChild(this.buildNoneKeywordsSection());
	        row3.el.appendChild(this.buildSearchWithinSection());
	        panel.el.appendChild(row1.el);
	        panel.el.appendChild(row2.el);
	        panel.el.appendChild(row3.el);
	        return panel.el;
	    };
	    AdvancedSearchPanel.prototype.buildSearchWithinSection = function () {
	        var wrapper = Coveo.$$('div', { id: 'left' });
	        var input = Coveo.$$('input', { className: 'coveo-advanced-search-panel-search-within-full', type: 'radio', name: 'searchType', value: 'full-text' });
	        input.setAttribute('checked', 'checked');
	        var input2 = Coveo.$$('input', { className: 'coveo-advanced-search-panel-search-within-title', type: 'radio', name: 'searchType', value: 'title' });
	        wrapper.el.innerHTML = 'Search within:<br/>' + input.el.outerHTML + 'Full Text' + '&nbsp;' + input2.el.outerHTML + 'Title Only';
	        return wrapper.el;
	    };
	    AdvancedSearchPanel.prototype.buildAllKeywordsSection = function () {
	        var wrapper = Coveo.$$('div', { id: 'left' });
	        var input = Coveo.$$('input', { className: 'coveo-advanced-search-panel-all-input' });
	        input.setAttribute('placeholder', 'All of these words');
	        wrapper.el.appendChild(input.el);
	        return wrapper.el;
	    };
	    AdvancedSearchPanel.prototype.buildExactPhraseSection = function () {
	        var wrapper = Coveo.$$('div', { id: 'right' });
	        var input = Coveo.$$('input', { className: 'coveo-advanced-search-panel-exact-input' });
	        input.setAttribute('placeholder', 'This exact phrase');
	        wrapper.el.appendChild(input.el);
	        return wrapper.el;
	    };
	    AdvancedSearchPanel.prototype.buildAnyKeywordsSection = function () {
	        var wrapper = Coveo.$$('div', { id: 'left' });
	        var input = Coveo.$$('input', { className: 'coveo-advanced-search-panel-any-input' });
	        input.setAttribute('placeholder', 'Any of these words');
	        wrapper.el.appendChild(input.el);
	        return wrapper.el;
	    };
	    AdvancedSearchPanel.prototype.buildNoneKeywordsSection = function () {
	        var wrapper = Coveo.$$('div', { id: 'right' });
	        var input = Coveo.$$('input', { className: 'coveo-advanced-search-panel-none-input' });
	        input.setAttribute('placeholder', 'None of these words');
	        wrapper.el.appendChild(input.el);
	        return wrapper.el;
	    };
	    AdvancedSearchPanel.prototype.showHidePanel = function () {
	        var panel = document.getElementById('coveo-advanced-search-panel');
	        if (panel.style.display === 'none' || panel.style.display == '') {
	            panel.style.display = 'table';
	        }
	        else {
	            panel.style.display = 'none';
	        }
	    };
	    AdvancedSearchPanel.ID = 'AdvancedSearchPanel';
	    AdvancedSearchPanel.options = {};
	    return AdvancedSearchPanel;
	}(Component));
	exports.AdvancedSearchPanel = AdvancedSearchPanel;
	Initialization.registerAutoCreateComponent(AdvancedSearchPanel);


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var Component = Coveo.Component;
	var Initialization = Coveo.Initialization;
	var ComponentOptions = Coveo.ComponentOptions;
	var DefaultOmniboxCustomSuggestionsTemplate_1 = __webpack_require__(8);
	/**
	 * This component is meant to be used inside a result template to display the URI or path to access a result.
	 */
	var OmniboxCustomSuggestions = (function (_super) {
	    __extends(OmniboxCustomSuggestions, _super);
	    /**
	     * Create a new OmniboxCustomSuggestions
	     * @param element
	     * @param options
	     * @param bindings
	     * @param result
	     */
	    function OmniboxCustomSuggestions(element, options, bindings, result) {
	        var _this = this;
	        _super.call(this, element, OmniboxCustomSuggestions.ID, bindings);
	        this.element = element;
	        this.options = options;
	        this.result = result;
	        this.options = ComponentOptions.initComponentOptions(element, OmniboxCustomSuggestions, options);
	        this.bind.onRootElement(Coveo.OmniboxEvents.populateOmnibox, function (args) { return _this.onPopulateOmnibox(args); });
	    }
	    OmniboxCustomSuggestions.prototype.onPopulateOmnibox = function (populateOmniboxObject) {
	        var _this = this;
	        console.log('=====> onPopulateOmnibox');
	        var q = populateOmniboxObject.completeQueryExpression.word;
	        // var querySuggest = {
	        //   q: q
	        // }
	        // let promiseQuerySuggest = new Promise((resolve, reject) => {
	        //   this.queryController.getEndpoint().getRevealQuerySuggest(querySuggest).done(function (data: Coveo.IRevealQuerySuggestResponse) {
	        //   });
	        // });
	        var self = this;
	        var query = {
	            q: q,
	            aq: this.options.expression,
	            firstResult: 0,
	            numberOfResults: this.options.numberOfResults,
	        };
	        var promise = new Promise(function (resolve, reject) {
	            _this.queryController.getEndpoint().search(query).done(function (data) {
	                if (data.totalCount > 0) {
	                    // console.log(data.totalCount);
	                    // console.log(data);
	                    var elementToReturn = self.buildOmniboxContent(data);
	                    var objectToReturn = {
	                        zIndex: -1000,
	                        element: elementToReturn
	                    };
	                    resolve(objectToReturn);
	                }
	                else {
	                    resolve({ element: undefined });
	                }
	            });
	        });
	        populateOmniboxObject.rows.push({
	            deferred: promise
	        });
	    };
	    OmniboxCustomSuggestions.prototype.buildOmniboxContent = function (results) {
	        var wrapper = Coveo.$$('div', { class: 'coveo-omnibox-custom-wrapper' });
	        var omniboxtemplate = this.options.customOmniboxResultTemplate;
	        _.each(results.results, function (res) {
	            var suggestion = Coveo.$$(omniboxtemplate.instantiateToElement(res));
	            wrapper.append(suggestion.el);
	        });
	        return wrapper.el;
	    };
	    OmniboxCustomSuggestions.ID = 'OmniboxCustomSuggestions';
	    OmniboxCustomSuggestions.options = {
	        expression: Coveo.ComponentOptions.buildStringOption({ defaultValue: '' }),
	        numberOfResults: Coveo.ComponentOptions.buildNumberOption({ defaultValue: 3 }),
	        customOmniboxResultTemplate: ComponentOptions.buildTemplateOption({
	            defaultFunction: function (e) { return new DefaultOmniboxCustomSuggestionsTemplate_1.DefaultOmniboxCustomSuggestionsTemplate(); },
	            selectorAttr: 'data-template-selector',
	            idAttr: 'data-template-id'
	        })
	    };
	    return OmniboxCustomSuggestions;
	}(Component));
	exports.OmniboxCustomSuggestions = OmniboxCustomSuggestions;
	Initialization.registerAutoCreateComponent(OmniboxCustomSuggestions);


/***/ }),
/* 8 */
/***/ (function(module, exports) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var Template = Coveo.Template;
	var DefaultOmniboxCustomSuggestionsTemplate = (function (_super) {
	    __extends(DefaultOmniboxCustomSuggestionsTemplate, _super);
	    function DefaultOmniboxCustomSuggestionsTemplate() {
	        _super.call(this);
	    }
	    DefaultOmniboxCustomSuggestionsTemplate.prototype.instantiateToString = function (queryResult) {
	        return '<div class="coveo-omnibox-selectable">Provide a template</div>';
	    };
	    return DefaultOmniboxCustomSuggestionsTemplate;
	}(Template));
	exports.DefaultOmniboxCustomSuggestionsTemplate = DefaultOmniboxCustomSuggestionsTemplate;


/***/ })
/******/ ])
});
;
//# sourceMappingURL=Coveo.Dell_emc.js.map